library("ggplot2")
save_as_pdf <- function(TARGET_FILE, plot_object, w = 4, h = 4)
{
    pdf(file = TARGET_FILE, width = w, height = h)
    print(plot_object)
    graphics.off()
}

options(crayon.enabled = FALSE)
suppressMessages(library(tidyverse))
suppressMessages(library(stringr))
df.exppaper3 <- read_csv("./dataset/EXPPAPER_FIG9/EXPPAPER_RANKS.csv.gz",
                    col_types=cols()) %>%
        mutate (Approach = case_when(Approach == "original" ~ "Original",
                                     Approach == "roundrobin" ~ "Round-robin",
                                     Approach == "greedy" ~ "Sorted-Greedy",
                                     TRUE ~ "Unknow")) %>%
        mutate (Approach = factor(Approach, levels = c("Original", "Round-robin", "Sorted-Greedy")))    
# Calculate durations
df.exppaper3 %>%
    group_by(Ranks, Approach, Rank) %>%
    # Real duration for each rank
    summarize(Duration.Total = sum(Duration), .groups = "drop") %>%
    ungroup() %>%
    group_by(Ranks, Approach) %>%
    # Ideal duration for the ranks of each case
    mutate(Ideal.Duration = sum(Duration.Total)/n()) %>%
    # Ranks distance from its ideal duration
    mutate(Local.Distance = Duration.Total - Ideal.Duration) %>%
    ungroup() -> df.durations

df.durations %>%
   filter(Ranks != 40 & Ranks != 80 & Ranks != 96) %>% 
   group_by(Approach, Ranks) %>% 
   summarize(maxY = max(abs(Local.Distance)), .groups = "drop") -> aux.y.lim

df.durations$Ranks %>% unique %>% 
    lapply(
        function(ranks, steps = 5){
            tibble(Ranks = rep(ranks, steps+1), Labels = c(seq(from = 0, by = ranks%/%steps, length.out = steps), ranks-1))
        }) %>% bind_rows()  %>%
    mutate(Set = as.integer(factor(as.character(Ranks)))) %>% 
    mutate(Breaks = Labels + Set*1000) %>% 
    select(-Ranks, -Set) -> scaleBreakLabels


df.durations %>%
    filter(Ranks != 40 & Ranks != 80 & Ranks != 96) %>%
    mutate(Set = as.integer(factor(as.character(Ranks)))) %>% 
    mutate(Rank2 = Rank + Set*1000) %>% 
    ggplot(aes(x=Rank2, y=Local.Distance)) + 
    geom_segment(aes(y=Local.Distance, yend=0, xend=Rank2), color="black", alpha=0.2, size=1) +
    geom_point(aes(x=Rank2, y=Local.Distance), colour="black", size=.8) + 
    geom_point(data = aux.y.lim, aes(y=maxY), x=1, colour="orange", size=2, alpha=0) + 
    geom_point(data = aux.y.lim, aes(y=-maxY), x=1, colour="orange", size=2, alpha=0) + 
    geom_hline(yintercept = 0, colour="blue") +
    facet_grid(Approach~Ranks, scales="free") +
    ylab("Distances from the mean time [s]") +
    #ylim(-max(abs(Local.Distance)), max(abs(Local.Distance))) +
    #scale_x_continuous("MPI Rank", breaks=seq(0,112,8)) +
    scale_x_continuous("Number of the worker",
                       breaks = scaleBreakLabels$Breaks,
                       labels = scaleBreakLabels$Labels) +
    theme_bw() +
    theme(
        legend.position="none",
        axis.text.x = element_text(color = "black", size = 9)
    ) -> plot

save_as_pdf("fig11.pdf", plot, w = 12, h = 4)
