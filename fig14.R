library("ggplot2")
save_as_pdf <- function(TARGET_FILE, plot_object, w = 4, h = 4)
{
    pdf(file = TARGET_FILE, width = w, height = h)
    print(plot_object)
    graphics.off()
}
library(magick)
magick_zize <- function(TARGET_FILE, plot_object, w=4, h=4)
{
    ggsave(TARGET_FILE,
           plot=plot_object,
           width=w,
           height=h,
           limit = FALSE)
    m_png <- image_trim(image_read(TARGET_FILE))
    image_write(m_png, TARGET_FILE)
    print(paste("See:", TARGET_FILE))
}
options(crayon.enabled = FALSE)
suppressMessages(library(tidyverse))
suppressMessages(library(stringr))
map(
    grep("exec-*", 
         list.files(path = "dataset/exp-speedup-new",
                    pattern = "traces_small.csv.gz",
                    full.names=TRUE,
                    recursive=TRUE),
         value = TRUE),     
    function(fl) {
        tryCatch(
            read_csv(fl, 
                     col_names = c("Rank", "Start", "End", "V4", "Operation"), 
                     col_types = cols (Rank = col_integer(), Start = col_double(), End = col_double(), Operation = col_character())),
           error = function(e){
               tibble(Rank = NA, Start = NA, End = NA, Operation = NA)
           }) %>% mutate(Origin = fl)
    })  %>% 
    bind_rows() -> df.times_raw2_0

df.times_raw2_0 %>% mutate(OriginDir = dirname(Origin)) %>% 
    separate(OriginDir, c("X1", "X2", "Exec"), sep="/") %>% 
    select(-X1, -X2) %>% 
    separate(Exec, c("X1", "ExecId", "Approach", "NProc"), sep="-") %>% 
    mutate(NProc = as.integer(NProc)) %>%
    select(-X1) -> df.times_raw2
df.times_raw2 %>% 
    filter(Operation == "iteration") %>% 
    group_by(ExecId, Approach, NProc) %>% 
    summarize(Start = min(Start), End = max(End), .groups="drop") %>% 
    mutate(Duration = End - Start) %>% 
    select(-Start, -End) -> df.times2

library(ggforce)

tibble(
    Case = c("Original",
             "Round-robin",
             "Sorted-Greedy",
             #"Original",
             "Round-robin",
             "Sorted-Greedy"),
    Proc = c("112 processes",
             "212 processes",
             "212 processes",
             #"212 processes",
             "112 processes",
             "112 processes"),
    Makespan = c(mean(df.times2 %>% filter(Approach == "original", NProc == 112) %>% .$Duration),
                 mean(df.times2 %>% filter(Approach == "roundrobin", NProc == 212) %>% .$Duration),
                 mean(df.times2 %>% filter(Approach == "greedy", NProc == 212) %>% .$Duration),
                 #mean(df.times2 %>% filter(Approach == "original", NProc == 212) %>% .$Duration),
                 mean(df.times2 %>% filter(Approach == "roundrobin", NProc == 112) %>% .$Duration),
                 mean(df.times2 %>% filter(Approach == "greedy", NProc == 112) %>% .$Duration)
                 )) %>%
    mutate(Max = max(Makespan)) %>%
    mutate(Diff = (Makespan*100/Max) - 100) %>%
    mutate(Show = ifelse(Diff != 0, TRUE, FALSE)) %>%
ggplot(aes(y = reorder(Case, Makespan), x = Makespan)) +
    geom_bar(stat='identity', fill = "gray", aes(color = Show)) +
    geom_segment(aes(x = Max, xend = Makespan, yend = Case, alpha = Show), 
                 size = 2, arrow = arrow(length = unit(0.2, "inches"))) +
    geom_text(aes(label = round(Makespan, 2)), angle = 90, size = 3, color = "black", vjust = -0.5) + 
    geom_text(aes(label = paste(round(Diff, 2), "%"), alpha = Show), x = 195, size = 4.5, color = "black", vjust = -0.5) + 
    theme_bw(base_size = 16) +
    theme(legend.position = "none", axis.text.y = element_text(hjust = 0.5)) +
    facet_col(~Proc, scales = "free_y", space = "free") +
    ylab("Gain over the Original \nbest parallel case") + 
    scale_alpha_discrete(range = c(0, 1)) + 
    scale_color_manual(values = c("black", "gray")) +
    xlim(c(0, NA))  -> p

save_as_pdf("fig14.pdf", p, w = 8, h = 4)
