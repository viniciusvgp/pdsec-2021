library("ggplot2")
save_as_pdf <- function(TARGET_FILE, plot_object, w = 4, h = 4)
{
    pdf(file = TARGET_FILE, width = w, height = h)
    print(plot_object)
    graphics.off()
}
read <- function(file) {
    read_csv(file,
             col_names=FALSE,
             progress=FALSE,
             col_types=cols()) %>%
        rename(Rank = X1, Ntriades = X2)
} 
options(crayon.enabled = FALSE)
suppressMessages(library(tidyverse))
tibble(DIR = c("./dataset/EXPPAPER/ntriades")) %>%
    mutate(FILE = map(DIR, function(dir) {
        list.files(dir,
                   pattern = "ntriades",
                   full.names=TRUE,
                   recursive=TRUE)
    })) %>%
    unnest(FILE) %>%
    mutate(DATA = map(FILE, read)) %>%
    select(-DIR) %>%
    unnest(DATA) %>%
    mutate(FILE = sub("ntriades/ntriades-", "", FILE)) %>%
    mutate(Approach = sub("-\\d+.csv", "", FILE)) %>%
    separate(Approach, c("XX1", "XX2", "XX3", "App"), sep="/") %>%
    select(-XX1, -XX2, -XX3) %>%
    rename(Approach = App) %>%
    mutate(Ranks = sub("(.*)-", "", FILE)) %>%
    mutate(Ranks = sub("\\.(.*)", "", Ranks)) %>%
    select(-FILE) %>%
    mutate(Ranks = as.integer(Ranks)) %>%
    filter(Ranks < 512) %>%
    mutate (Approach = case_when(Approach == "original" ~ "Original",
                                 Approach == "roundrobin" ~ "Round-robin",
                                 Approach == "greedy" ~ "Sorted-Greedy",
                                 TRUE ~ "Unknow")) %>%
    mutate (Approach = factor(Approach, levels = c("Original", "Round-robin", "Sorted-Greedy"))) -> df.ntriades


df.ntriades %>% filter(Ranks %in% c(128, 256)) %>% 
    group_by(Approach, Rank, Ranks) %>% summarize(Total = sum(Ntriades), .groups = "drop") %>% ungroup -> df.hist

df.hist %>%
    group_by(Approach, Ranks) %>% summarize(Mean = mean(Total), .groups = "drop") -> df.mean.line
    
df.hist %>%
    ggplot(aes(x=Total)) + 
    geom_histogram(bins=30, alpha=.5, color = "black") +
    geom_vline(data = df.mean.line, aes(xintercept=Mean),   # Ignore NA values for mean
               linetype="dashed", size=1) +
    theme_bw(base_size=20) + facet_grid(Ranks~Approach, scales = "free_y",) +
    theme(legend.position = "top",
          legend.justification = 0,
          legend.margin = margin(0, 0, -0.5, 0, "cm"),
          plot.margin = margin(0, 0, 0, 0, "cm"),
          axis.text.x = element_text(color="black", size = 14, angle = 30)) +
    ylab("Count") +
    xlab("Number of S-R pairs for each worker") -> plot

save_as_pdf("fig8.pdf", plot, w = 9.5, h = 4)
