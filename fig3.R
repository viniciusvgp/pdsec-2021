dep0 <- "./dataset/FIG2-3/cmp_location.csv"
library(tidyverse)
read_delim(dep0, delim=" ", col_types=cols()) %>%
    filter(coordinate == "x") %>%
    mutate(value = lead(value)) %>% filter(!is.na(value)) %>%
    pull(value) -> xs
read_delim(dep0, delim=" ", col_types=cols()) %>%
    filter(coordinate == "y") %>%
    mutate(value = lead(value)) %>% filter(!is.na(value)) %>%
    unique %>%
    pull(value) -> ys
expand.grid(xs, ys) %>%
    rename(x = Var1, y = Var2) %>%
    as_tibble %>%
    # Here starts the very important part (VIP)
    arrange(y, -x) %>%
    mutate(cmp = n():1-1) %>%
    # Here stops the very important part (VIP)
    select(cmp, x, y) -> df.cmp
dep0 <- "./dataset/FIG2-3/mapping_data_to_cmp.csv"
options(crayon.enabled = FALSE)
suppressMessages(library(tidyverse))
suppressMessages(library(stringr))
df.resources <- read_csv(dep0, progress=FALSE) %>%
    mutate(receptor = as.factor(receptor)) %>%
    filter(cmp != -1) %>%
    left_join(df.cmp, by="cmp") 
figure_file_name <- "fig3.png"
my_cmp = c(5, 177) #, 110, 198, 205, 327, 410, 450)

df.resources %>%
    mutate(highlight = cmp %in% my_cmp) -> df.resources_sel

df.resources_sel %>%
    group_by(cmp) %>%
    summarize(Number.of.Combinations.Rx.Src = n(),
              .groups="drop") -> df.cmp_rx_src_observations

bind_rows(
    df.resources_sel %>%
    select(cmp, x, y, highlight) %>%
    distinct %>%
    pivot_longer(cmp),

    df.resources_sel %>%
    select(rx=receptor, x=rx_x, y=rx_y, highlight) %>%
    mutate(rx=as.integer(gsub("Rx_", "", rx))) %>%
    distinct %>%
    pivot_longer(rx),

    df.resources_sel %>%
    select(src, x=src_x, y=src_y, highlight) %>%
    distinct %>%
    pivot_longer(src)
) -> df.unique.cmp_rx_src

df.unique.cmp_rx_src %>%
    filter(name == "cmp") %>%
    pivot_wider(names_from=name, values_from=value) %>%
    left_join(df.cmp_rx_src_observations, by="cmp") %>%
    mutate(highlight = cmp %in% my_cmp) -> df.prep

# Segments selection
df.resources_sel %>%
    filter(cmp %in% my_cmp) %>%
    select(cmp, rx_x, rx_y, src_x, src_y) -> df.seg

ggplot() +
    # rx src combinations
    geom_segment(data=df.seg %>% mutate(src_y = -1 * src_y,
                                        rx_y = -1 * rx_y),
                 aes(x=src_x,
                     y=src_y,
                     xend=rx_x,
                     yend=rx_y), alpha=.04) +
    # big rx points
    geom_point(data=df.unique.cmp_rx_src %>% filter(name == "rx") %>% 
                   mutate(y = -1 * y),
               aes(x=x, y=y), #, alpha=highlight),
               shape = "triangle",
               color = "red",
               size = 6) +
    # small src points
    geom_point(data=df.unique.cmp_rx_src %>% filter(name == "src") %>% 
                   mutate(y = -1 * y),
               aes(x=x, y=y), #, alpha=highlight),
               shape = "square",
               color = "blue",
               size = .7,
               height = 20) +
    # cmp points
    geom_point(data=df.prep %>% 
                   mutate(y = -1 * y),
               aes(x=x, y=y, size=Number.of.Combinations.Rx.Src, color=highlight, alpha=highlight)) +
    # cmp labels
    geom_text(data=df.prep %>% filter(cmp%in%my_cmp) %>% 
                   mutate(y = -1 * y), aes(x=x, y=y, label=Number.of.Combinations.Rx.Src, alpha=highlight), size=5, color="blue") +
    # cosmetics
    coord_cartesian(xlim=c(0-20000*.05, 20000+20000*0.05)) +
    coord_cartesian(xlim=c(0-20000*.05, 10000),
                    ylim=c(7500, 0-1000)) +
    theme_bw(base_size = 24) +
    scale_alpha_manual(values = c("FALSE" = 0.1, "TRUE" = 1), guide = "none") +
    guides(size = guide_legend(title = "Number of S-R Pairs")) +
    scale_size(range=c(5, 15)) +
    scale_color_manual(values = c("black", "green"), guide = "none") +
    theme(
        legend.position = "top",
        legend.justification = "left",
        legend.box.spacing = unit(0, "pt"),
        legend.box.margin = margin(0, 0, 0, 0)
    ) +
    xlab("East-West direction (meters)") +
    ylab("North-South direction (meters)") +


    annotate("segment", x= 2300, xend = 2500, yend = 500, y = -1000,
             color="black", arrow = arrow(length = unit(0.5, "cm")), size = 1.1
             ) + 
    annotate("label", x= 2300, y = -1000, label="A",
             label.size=0.805,
             size= 6,
             ) -> p

library(magick)
magick_zize <- function(TARGET_FILE, plot_object, w=4, h=4)
{
    ggsave(TARGET_FILE,
           plot=plot_object,
           width=w,
           height=h,
           limit = FALSE)
    m_png <- image_trim(image_read(TARGET_FILE))
    image_write(m_png, TARGET_FILE)
    print(paste("See:", TARGET_FILE))
}
dir.create("./img/", showWarnings = FALSE, recursive = TRUE)
TARGET_FILE <- figure_file_name
magick_zize(TARGET_FILE,
            p,
            w=12, h=8)
