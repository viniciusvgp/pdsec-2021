library(tidyverse)
workers <- seq(1, 8)
tibble(Load = c(rep(1200, 6),
                rep( 960, 6),
                rep( 680, 4),
                rep( 320, 2))) %>%
    arrange(-Load) %>%
    mutate(Order = 1:n()) %>%
    # RR "automatically"
    bind_cols(
        tibble(RR = rep(workers, 10)) %>%
        slice(1:18)
    ) %>%
    # Greedy "manually" (thanks Jessica)
    bind_cols(
        tibble(GR = c(seq(1, 8), 1, 2, 7, 8, 3, 4, 5, 6, 3, 4))
    )  -> df.fake_load
    # Original "kind of manually"
    # won't  reflect reality because CMPs are ordered
    #bind_cols(
    #    tibble(OR = c(unlist(lapply(workers, function(x) { rep(x, 2) })), 8, 8))
    #) %>%

figure_file_name <- "fig6.png"
df.fake_load %>%
    ggplot() +
    theme_bw(base_size=24) +
    geom_rect(aes(xmin=Order-0.4, xmax=Order+0.4, ymin=0, ymax=Load, fill=as.factor(Load)), alpha=.8) +
    scale_x_continuous(breaks = seq(1, 18)) +
    theme_bw(base_size = 24) +
    theme(
        legend.position = "none",
        legend.justification = "left",
        legend.box.spacing = unit(0, "pt"),
        legend.box.margin = margin(0, 0, 0, 0)
    ) +
    scale_fill_brewer(palette = "Set1") +
    geom_text(aes(x=Order, y=Load-Load*0.05-100, fontface="bold", label=Load, angle = 90, size=16)) +
    xlab("CMP") +
    ylab("Number of\nS-R Pairs") +
    theme(
        axis.title.y = element_text(size=20)
    ) -> p

library(magick)
magick_zize <- function(TARGET_FILE, plot_object, w=4, h=4)
{
    ggsave(TARGET_FILE,
           plot=plot_object,
           width=w,
           height=h,
           limit = FALSE)
    m_png <- image_trim(image_read(TARGET_FILE))
    image_write(m_png, TARGET_FILE)
    print(paste("See:", TARGET_FILE))
}
dir.create("./img/", showWarnings = FALSE, recursive = TRUE)
TARGET_FILE <- figure_file_name
magick_zize(TARGET_FILE,
            p,
            w=12, h=3)
