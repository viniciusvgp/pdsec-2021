library("ggplot2")
save_as_pdf <- function(TARGET_FILE, plot_object, w = 4, h = 4)
{
    pdf(file = TARGET_FILE, width = w, height = h)
    print(plot_object)
    graphics.off()
}

# expectation
spe <- function(x, y){
    mean(x) * mean(1/y)
}

# variance
spvar <- function(x, y){
    (mean(x**2) * mean(1/(y**2))) - ((mean(x)**2) * (mean(1/y)**2))
}

# return the sd multiplier
conf <- function(values, x = 0.1, confidence = 0.95, step = 0.1){
    m = mean(values)
    sdev = sd(values)

    start = m - x * sdev
    end = m + x * sdev

    r = sum(values >= start & values <= end)

    if (r >= confidence * length(values))
        return(x)
    else 
        conf(values, x + step, confidence, step)
}

options(crayon.enabled = FALSE)
suppressMessages(library(tidyverse))
suppressMessages(library(stringr))
map(
    grep("exec-*", 
         list.files(path = "dataset/exp-speedup-new",
                    pattern = "traces_small.csv.gz",
                    full.names=TRUE,
                    recursive=TRUE),
         value = TRUE),     
    function(fl) {
        tryCatch(
            read_csv(fl, 
                     col_names = c("Rank", "Start", "End", "V4", "Operation"), 
                     col_types = cols (Rank = col_integer(), Start = col_double(), End = col_double(), Operation = col_character())),
           error = function(e){
               tibble(Rank = NA, Start = NA, End = NA, Operation = NA)
           }) %>% mutate(Origin = fl)
    })  %>% 
    bind_rows() -> df.times_raw2_0

df.times_raw2_0 %>% mutate(OriginDir = dirname(Origin)) %>% 
    separate(OriginDir, c("X1", "X2", "Exec"), sep="/") %>% 
    select(-X1, -X2) %>% 
    separate(Exec, c("X1", "ExecId", "Approach", "NProc"), sep="-") %>% 
    mutate(NProc = as.integer(NProc)) %>%
    select(-X1) -> df.times_raw2
df.times_raw2 %>% 
    filter(Operation == "iteration") %>% 
    group_by(ExecId, Approach, NProc) %>% 
    summarize(Start = min(Start), End = max(End), .groups="drop") %>% 
    mutate(Duration = End - Start) %>% 
    select(-Start, -End) -> df.times2
baselineList2 <- df.times2 %>% filter(Approach == "original", NProc == 1) %>% .$Duration

df.times2 %>%
    ungroup() %>%
    mutate(NProc = as.integer(NProc)) %>% 
    mutate (Approach = case_when(Approach == "original" ~ "Original", 
                                 Approach == "roundrobin" ~ "Round-robin",
                                 Approach == "greedy" ~ "Sorted-Greedy", 
                                 TRUE ~ "Unknow")) %>%
    mutate(Approach = factor(Approach, levels = c("Original", "Round-robin", "Sorted-Greedy"))) %>%
    filter(ExecId != 64) %>%
    group_by(Approach, NProc) %>% 
    filter(!is.na(Approach)) %>%    # to deal with scorep issues
    summarize(SU = spe(baselineList2, Duration), 
              Var = spvar(baselineList2, Duration),
              SDmultiplier = conf(Duration, confidence = 0.997), .groups="drop") %>% 
    ungroup() -> df.speedup_new_conf

brks <- df.times2 %>% filter(NProc > 2) %>% pull(NProc) %>% unique %>% sort

df.speedup_new_conf %>%
    mutate(sd = sqrt(Var)) %>% 
    mutate(error = SDmultiplier*sd) %>%
    ggplot(aes(x = NProc, y = SU, color = Approach)) +
    annotate("segment", x = 1, xend = max(df.speedup_new_conf$NProc), y = 1, yend = max(df.speedup_new_conf$NProc), linetype = "dashed") +
    geom_line(alpha=0.7) + 
    geom_point(size = 2) + 
    geom_errorbar(aes(ymin=SU-error,
                      #ymax=SU+error), width=1, color='black') +
                      ymax=SU+error), width=0.1, color='black') +
    geom_vline(xintercept = 112, color="deeppink", linetype="dotted") +
    ylab("Speedup") +
    xlab("Number of MPI processes") +
    theme_bw(base_size = 16) +
    #scale_x_discrete(guide = guide_axis(n.dodge=2)) +
    scale_x_continuous(breaks=c(0, brks)) +
    theme(
        panel.grid.minor = element_blank(),
        legend.position = "top",
        legend.justification = "left",
        legend.box.spacing = unit(0, "pt"),
        legend.box.margin = margin(0, 0, 0, 0)
    ) -> p 
    # coord_cartesian(ylim = c(0,max(df.speedup_new_conf$SU))) -> p
p -> plot

save_as_pdf("fig13.pdf", plot, w = 8, h = 5)
